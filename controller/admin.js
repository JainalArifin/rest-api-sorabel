var Admin = require("../models/Admin");
var bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = {
  get: (req, res) => {
    Admin.find((err, data) => {
      // Note that this error doesn't mean nothing was found,
      // it means the database had an error while searching, hence the 500 status
      if (err) return res.status(500).send(err)
      // send the list of all people
      return res.status(200).send(data);
    })
  },
  signup: (req, res) => {
    bcrypt.hash(req.body.password, saltRounds, (err, hash) => {
      Admin.create({
        username: req.body.username,
        password: hash
      })
        .then((data) => {
          res.status(200).send(data)
        })
        .catch((err) => {
          res.send(err)
        })
    })
  },
  signin: (req, res) => {
    Admin.findOne({
       username: req.body.username
    }).then((user) => {
      console.log("=== user =>", user)
      if (!user) {
        res.send("gagal")
      } else {
        bcrypt.compare(req.body.password, user.password, (err, result) => {
          if (result == true) {
            res.send({
              message: "succes",
              result: result
            })
          } else {
            res.send('Incorrect password');
          }
        })
      }
    })
  },
  update: (req, res) => {

    Admin.findOneAndUpdate({ _id: req.params.id }, { $set: req.body }, { new: true }).then((docs) => {
      if (docs) {
        res.send({ success: true, data: docs });
      } else {
        res.send({ success: false, data: "no such user exist" });
      }
    }).catch((err) => {
      res.send(err);
    })
  },
  delete: (req, res) => {
    Admin.remove({ _id: req.params.id })
      .then((docs) => {
        if (docs) {
          res.send({ "success": true, data: docs });
        } else {
          res.send({ "success": false, data: "no such user exist" });
        }
      }).catch((err) => {
        res.send(err);
      })
  }
}